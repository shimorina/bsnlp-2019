Data, scripts, experiments for the paper

**[_Creating a Corpus for Russian Data-to-Text Generation Using Neural Machine Translation and Post-Editing_](https://www.aclweb.org/anthology/W19-3706). A. Shimorina, E. Khasanova, C. Gardent. Balto-Slavic NLP Workshop at ACL 2019.** [[slides](bsnlp2019_slides.pdf)]

```bibtex
@inproceedings{shimorina2019webnlg-ru,
    title = "Creating a Corpus for {R}ussian Data-to-Text Generation Using Neural Machine Translation and Post-Editing",
    author = "Shimorina, Anastasia  and Khasanova, Elena  and Gardent, Claire",
    booktitle = "Proceedings of the 7th Workshop on Balto-Slavic Natural Language Processing",
    month = aug,
    year = "2019",
    address = "Florence, Italy",
    publisher = "Association for Computational Linguistics",
    url = "https://www.aclweb.org/anthology/W19-3706",
    pages = "44--49"
}
```

# WebNLG-ru

**The latest release of the post-edited Russian corpus is in the [WebNLG repository](https://gitlab.com/shimorina/webnlg-dataset/-/tree/master/release_v3.0/ru). See this [README](https://gitlab.com/shimorina/webnlg-dataset/-/blob/master/release_v3.0/README.md) for data description.**

WebNLG-ru was created using `release_v2 ` of [WebNLG-en data](https://gitlab.com/shimorina/webnlg-dataset). See WebNLG-en official [website](https://webnlg-challenge.loria.fr/) and [documentation](https://webnlg-challenge.loria.fr/docs/).

## Docs
WebNLG maps RDF triples to text.

WebNLG-ru data instance looks like this:

```xml
<entry category="Astronaut" eid="Id7" part="dev" size="1">
  <modifiedtripleset>
    <mtriple>Elliot_See | occupation | Test_pilot</mtriple>
  </modifiedtripleset>
  <lex lang="en" lid="Id2">Elliot See served as a test pilot.</lex>
  <lex lang="ru" lid="Id2">Elliot See служил тестовым пилотом.</lex>
  <finallex comment="ne;voc:term" lang="ru" lid="Id2">Эллиот Си служил летчиком-испытателем.</finallex>

  <lex lang="en" lid="Id3">Elliot See flew as a test pilot.</lex>
  <lex lang="ru" lid="Id3">Elliot See вылетел в качестве летчика-испытателя.</lex>
  <finallex comment="ne;gr:asp" lang="ru" lid="Id3">Эллиот Си летал в качестве летчика-испытателя.</finallex>
</entry>
```

* `<entry>`, `<modifiedtripleset>`, `<lex lang='en'>` are taken from WebNLG-en (`release_v2`). See documentation [here](https://webnlg-challenge.loria.fr/docs/). Train, dev, test parts (`part`) are in the same file in WebNLG-ru, but the entry ids (`eid`) were kept as they are in WebNLG-en.

* `<lex lang="ru">` contains a machine-translated text from `<lex lang="en">`.

* `<finallex>` contains a post-edited text. The `comment` attribute indicates types of errors encountered. See [error classification](./classification/) for more detail.

Currently, the files for University, Monument, ComicsCharacter, Astronaut were post-edited manually (1 size) and using rules (2-7 sizes). In the size 1 for those categories, if `<finallex>` is empty, it means that an MT-sentence was correct (no post-editing was needed).

Some data entries have links between English and Russian names of subjects and objects from `<modifiedtripleset>` if the predicate `sameAs` was available in DBpedia.

```xml
<entry category="Astronaut" eid="Id2" part="test" size="1">
  <modifiedtripleset>
    <mtriple>Apollo_8 | backup pilot | Buzz_Aldrin</mtriple>
  </modifiedtripleset>
  ...
  <dbpedialinks>
    <dbpedialink direction="en2ru">Buzz_Aldrin|sameAs|Олдрин,_Базз</dbpedialink>
    <dbpedialink direction="en2ru">Apollo_8|sameAs|Аполлон-8</dbpedialink>
  </dbpedialinks>
</entry>
```

## Experiments

All the experimental findings reported in the paper were obtained using the following scripts and data.

* `code`
	* `benchmark_reader_finallex_dbpedialinks.py` reads WebNLG-ru files
	* `annotation_stats.py` outputs some descriptive statistics on annotation errors
	* `agreement.py` calculates agreement between two coders
	* `prepare_input_for_nmt.py` generates input files for OpenNMT training
	* `create_replacement_dicts.py` generates csv files with possible replacement rules
	* `rule_based_correction.py` take replacement rules and applies them to sizes 2-7
	* `distance.py` code was taken from [here](https://github.com/liaocyintl/detail_edit_distance).


* `agreement`

	Error annotations of two coders. Output files with agreement scores.

* `classification`

	Detailed error annotation scheme with examples. General guidelines for post-editing WebNLG data.

* `rule-based_pe`
	
	Extracted rules (initial and manually corrected (`_good`)) for modifying MT-sentences.

* `ape`

	Input files for neural training. Hyperparameters, the model, the bash script to run OpenNMT-tf and to calculate BLEU.

### Requirements
Code tested with Python3.6

```
pip install -r requirements.txt
```

## WebNLG License

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
