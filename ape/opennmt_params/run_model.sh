#!/bin/bash

modelpath=smallNMT-custom-adam0.0005-default/  # where to save the model
configfile=data.smallnmt.custom.yml
filespath=/your/path/to/the/opennmt_inputs/  # where input files are stored

# build vocabulary
# onmt-build-vocab --size 50000 --save_vocab src-vocab.txt train.tok.mt
# onmt-build-vocab --size 50000 --save_vocab tgt-vocab.txt train.tok.pe

onmt-main train_and_eval --model custom_smallnmt.py --auto_config --config ${configfile}


for epoch in 3000 # add other epochs if necessary
do
	for part in train dev test
	do
			onmt-main infer \
			--config ${configfile} \
			--features_file ${part}.tok.mt \
			--predictions_file ${modelpath}/output.${epoch}.${part}.tok \
			--checkpoint_path ${modelpath}model.ckpt-${epoch}
			# need to have detokenised outputs
			# cat ${modelpath}/output.${epoch}.${part}.detok | sacrebleu --tokenize 13a ${filespath}/${part}.pe > ${modelpath}/sacrebleu.${part}.${epoch}step.txt
	done
done


# bleu; calculated with NLTK on tokenised data (install compare-mt via pip)
compare-mt train.tok.pe train.tok.mt output.3000.train.tok --compare_scores score_type=bleu,bootstrap=1000,prob_thresh=0.05
compare-mt dev.tok.pe dev.tok.mt output.3000.dev.tok --compare_scores score_type=bleu,bootstrap=1000,prob_thresh=0.05
compare-mt test.tok.pe test.tok.mt output.3000.test.tok --compare_scores score_type=bleu,bootstrap=1000,prob_thresh=0.05


# see logging with tensorboard
# tensorboard --logdir="smallNMT-custom-adam0.0005-default/"
