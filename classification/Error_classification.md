## Post-Editing Error Classification
_Error types and error tags with extensions_

### 1. **gr** --- grammar

 * **cm** --- case marking (syntactic dependencies)

_Example 1:_
> **Original sentence**: Alan Bean was a test pilot. 
> **Machine translation:** Алан Бин был летчик-испытатель.
> **Edited translation:** Алан Бин был летчиком-испытателем.
> **Comment:** Nominative ---> Instrumental

_Example 2:_
> **Original sentence**:The creator of Blockbuster (comics character) is Carmine Infantino.
> **Machine translation:** Создателем Blockbuster (персонажем комиксов) является Кармин Инфантино.
> **Edited translation:** Создателем Blockbuster (персонажа комиксов) является Кармайн Инфантино.
> **Comment:** Instrumental ---> Genitive

* **cop** --- copula omitted or preserved erroneously

_Example 1:_
> **Original sentence**: Elliot See is from the United States.
> **Machine translation:** Эллиот Зе является из США.
> **Edited translation:** Эллиот Си из США.
> **Comment:** wrongly preserved and translated сopula "is" = "является"

_Example 2:_
> **Original sentence**: The starring of Big Hero 6 (film) is Maya Rudolph.
> **Machine translation:** В главной роли Big Hero 6 (фильм) находится Майя Рудольф.
> **Edited translation:** В главной роли в Big Hero 6 (фильм) - Майя Рудольф.
> **Comment:** wrongly preserved and translated сopula "is" = "находится"

_Example 3:_
> **Original sentence**: William Anders was an American.
> **Machine translation:** Уильям Андерс американец.
> **Edited translation:** Уильям Андерс был американцем.
> **Comment:** wrongly omitted copula "was"


* **have** - erroneously omitted or translated

_Example 1:_
> **Original sentence**: California have fossils of the Smilodon.
> **Machine translation:** В Калифорнии окаменелости Смилодона.
> **Edited translation:** В Калифорнии есть окаменелости смилодона.
> **Comment:** wrongly omitted "have"

_Example 2:_
> **Original sentence**: Adams County, Pennsylvania has Carroll County, Maryland to its southeast.
> **Machine translation:** Adams County, Пенсильвания имеет Carroll County, Мэриленд на своем юго-востоке.
> **Edited translation:** Округ Адамс, Пенсильвания, граничит с округом Кэрролл, Мэриленд, на юго-востоке.
> **Comment:** wrongly translated "have"

* **asp** -- errors with verbal aspect

_Example 1:_
> **Original sentence**: Abel Hernandez played for the Uruguay Olympic football team.
> **Machine translation:** Абель Эрнандес сыграл за сборную Олимпийских игр в Уругвае.
> **Edited translation:** Абель Эрнандес играл за олимпийскую сборную Уругвая по футболу.
> **Comment:** perfective ---> imperfective

* **pr** --- preposition: wrong preposition or wrongly preserved preposition, usually linked with case marking

_Example 1:_
> **Original sentence**: William Anders serves as Ambassador to Norway in the United States.
> **Machine translation:** Уильям Андерс является послом в Норвегии в Соединенных Штатах.
> **Edited translation:** Уильям Андерс является послом Соединенных Штатов в Норвегии.
> **Comment:** wrongly translated preposition 'to' as 'в' (in)

_Example 2:_
> **Original sentence**: Buzz Aldrin was born with the name Edwin Eugene Aldrin Jr.
> **Machine translation:** Buzz Aldrin родился с именем Эдвин Юджин Олдрин младший.
> **Edited translation:** Базз Олдрин родился под именем Эдвин Юджин Олдрин младший.
> **Comment:** literally translated preposition 'with' as 'с'


* **attr** --- attribution, usually with multiple or distant nominal attributes

_Example 1:_
> **Original sentence**: The university of texas at Austin is affiliated to the university of Texas system.
> **Machine translation:** Университет Техаса в Остине связан с университетом системы Texas.
> **Edited translation:** Университет Техаса в Остине связан с системой университета Техаса.
> **Comment:** the university of the system of Texas ---> the system of the university of Texas

_Example 2:_
> **Original sentence**: The creator of Airman (comics character) is Harry Sahle.
> **Machine translation:** Создатель Airman (комикс-персонаж) - Гарри Сахле.
> **Edited translation:** Создателем Airman (персонажа комиксов) является Гарри Сале.
> **Comment:** comics-character ---> character of comics

* **ps** --- possessive case, especially with multiple attributes

_Example 1:_
> **Original sentence**: The University of Texas at Austin's mascot is called Hook'em.
> **Machine translation:** Техасский университет в талисмане Остина называется Hook'em.
> **Edited translation:** Талисман Техасского университета в Остине называется Hook'em.
> **Comment:** Austin's mascot ---> university ... 's mascot

* **pos** --- wrongly chosen part-of-speech in case of ambiguity

_Example 1:_
> **Original sentence**: Graeme Garden stars in Bananaman.
> **Machine translation:** Звезды Грэм Саун в Бананамане.
> **Edited translation:** Грэм Гарден снимается в Бананамане.
> **Comment:** stars (noun) ---> stars (verb)

_Example 2:_
> **Original sentence**: Duncan Rouleau is American.
> **Machine translation:** Дункан Руло - американский.
> **Edited translation:** Дункан Руло - американец.
> **Comment:** American (adj) ---> American (noun)

* **ga** --- grammatical agreement (usually gender or number)

_Example 1:_
> **Original sentence**: Alan Bean's birth name was Alan LaVern Bean.
> **Machine translation:** Родовым именем Алана Бина был Алан Лаверн Бин.
> **Edited translation:** Родовым именем Алана Бина было Алан Лаверн Бин.
> **Comment:** masculine verb ---> neuter

_Example 2:_
> **Original sentence**: The Senator representing California was Dianne Feinstein.
> **Machine translation:** Сенатор, представляющий Калифорнию, был Дайанн Фейнстейн.
> **Edited translation:** Сенатором, представляющим Калифорнию, была Дайанн Файнстайн.
> **Comment:** masculine verb ---> feminine

* **v** --- voice, intentionality

_Example 1:_
> **Original sentence**: The United States Navy awarded the Distinguished Service Medal to Alan Shepard.
> **Machine translation:** Военно-морской флот Соединенных Штатов награжден медалью «За выдающиеся заслуги» Алану Шепарду.
> **Edited translation:** Военно-морской флот Соединенных Штатов наградил медалью «За выдающуюся службу» Алана Шепарда.
> **Comment:** passive ---> active

_Example 2:_
> **Original sentence**: Bananaman last aired on 15th April 1986.
> **Machine translation:** 15 мая 1986 года последний эфир транслировался бананаманом.
> **Edited translation:** 15 мая 1986 года была последняя трансляция Бананамана.
> **Comment:** patient rendered as subject

* **self** --- special Russian pronoun “свой” wrongly translated as “его”, “её”

_Example 1:_
> **Original sentence**: William Anders received a M.S. from his alma Mater, AFIT.
> **Machine translation:** Уильям Андерс получил M.S. от его альма-матер, AFIT.
> **Edited translation:** В 1962 году Уильям Андерс получил M.S. своей альма-матер, AFIT.

### 2. **voc** --- lexical mistakes

* **amb** --- ambiguity: polysemous vocables, homonyms, false friends

_Example 1:_
> **Original sentence**: Alan Shepard's occupation was a test pilot.
> **Machine translation:** Завоевание Алана Шепарда было летчиком-испытателем.
> **Edited translation:** По профессии Алан Шепард был летчиком-испытателем.
> **Comment:** wrong lexeme for the vocable "occupation"

_Example 2:_
> **Original sentence**: Big Hero 6 was Baymax's first film appearance.
> **Machine translation:** Big Hero 6 был первым видом фильма Baymax.
> **Edited translation:** Big Hero 6 был первым появлением на экране для героя Baymax.
> **Comment:** wrong lexeme for the vocable "appearance"

_Example 3:_
> **Original sentence**: Buzz Aldrin walked in space 52.0 minutes.
> **Machine translation:** Базз Олдрин зашел в космос на 52,0 минуты.
> **Edited translation:** Базз Олдрин находился в космосе 52,0 минуты.
> **Comment:** wrongly translated verb of motion "walk" as "walk into" instead of "walk" as "spend time"

* **colloc** --- collocations (depend on another word in a context): ignoring the context, literally translated collocates

_Example 1:_
> **Original sentence**: Buzz Aldrin won the Legion of Merit ribbon.
> **Machine translation:** Базз Олдрин выиграл ленту «Легион за заслуги».
> **Edited translation:** Базз Олдрин получил ленту «Легион почета».
> **Comment:** a different collocation in Russian was needed: "received a ribbon" instead of "won a ribbon"

_Example 2:_
> **Original sentence**: Scott Adsit provides the voice for the character Baymax.
> **Machine translation:** Скотт Адсит обеспечивает голос за персонажа "Баймакс".
> **Edited translation:** Скотт Адсит озвучивает персонажа Баймакса.
> **Comment:** literally translated collocates "provide" and voice" instead of finding a matching equivalent "озвучивать" (to voice)

* **term** --- terminology - literaly tranlslated terms, misrecognized terminology

_Example 1:_
> **Original sentence**: William Anders is a test pilot.
> **Machine translation:** Уильям Андерс - экспериментальный пилот.
> **Edited translation:** Уильям Андерс - летчик-испытатель.
> **Comment:** literally translated parts of the term "test pilot" instead of matching it with the equivalent term in Russian

* **var** --- errors related to language variation (British or American) 
_Example 1:_
> **Original sentence**: Dallas is a county in Texas.
> **Machine translation:** Даллас - это графство в Техасе.
> **Edited translation:** Даллас - это округ в Техасе.
> **Comment:** British variant for "county" used for an American territorial unit

* **wr** --- word translated incorrectly with no basisf for this (such as polysemy, collocation, terminology, etc.)
_Example 1:_
> **Original sentence**: Alba Iulia is part of Alba County.
> **Machine translation:** "Альба Юлия" входит в состав компании "Альба".
> **Edited translation:** Алба-Юлия входит в состав жудеца Албы.
> **Comment:** "county" wrongly translated as "company"

* **mw** --- multiword expression not treated as such (usually of the "noun-noun" form)

_Example 1:_
> **Original sentence**: In 1962, William Anders received a M.S. from his alma mater, AFIT.
> **Machine translation:** В 1962 году Уильям Андерс получил М.С. от своей альмы Матер, AFIT.
> **Edited translation:** В 1962 году Уильям Андерс получил степень магистра естественных наук своей альма-матер, AFIT.
> **Comment:** two parts of a multiword expression treated separately

### 3.  **ne** --- named entity related
wrong transliteration or not translated, subcategories for frequent and more specific cases
* **ne** - all non-specific ne cases
_Example 1:_
> **Original sentence**: William Anders graduated, with an M.S., from AFIT in 1962.
> **Machine translation:** William Anders закончила, с М.С., из AFIT в 1962 году.
> **Edited translation:** Уильям Андерс выпустился из AFIT со степенью магистра естественных наук в 1962 году.
> **Comment:** name preserved in Latin alphabet should be transliterated to Cyrillic

* **wt** --- wrongly translated named entity for which there is no correspondence in the target language

_Example 1:_
> **Original sentence**: The Hook 'em is the mascot of University of Texas at Austin.
> **Machine translation:** «Крюк» - это талисман Техасского университета в Остине.
> **Edited translation:** «Hook'Em» - это талисман Техасского университета в Остине.
> **Comment:** name "Hook'Em" should not be translated as "Hook"

* **nt** --- not translated names or their parts that should be translated

_Example:_
> **Original sentence**: The Purple Finch is a New Hampshire bird.
> **Machine translation:** Фиолетовый Финч - птица Нью-Хэмпшира.
> **Edited translation:** Фиолетовый зяблик - птица из Нью-Гэмпшира
> **Comment:** an equivalent for the "purple finch" in Russian should be used

* **abr** --- abbreviations

_Example:_
> **Original sentence**: William Anders graduated, with an M.S., from AFIT in 1962.
> **Machine translation:** Уильям Андерс закончил с М.С., из AFIT в 1962 году.
> **Edited translation:** Уильям Андерс окончил AFIT со степенью магистра естественных наук в 1962 году.
> **Comment:** common acronym M.S. is transliterated as non-existing acronym "М.С"

* **exp** --- omitted explanatory word

_Example:_
> **Original sentence**: Buzz Aldrin graduated in 1963 from MIT with a Sc.D.
> **Machine translation:** Buzz Aldrin окончил в 1963 году из MIT с Sc.D.
> **Edited translation:** Базз Олдрин окончил в 1963 году MIT со степенью доктора естественных наук.
> **Comment:** in Russian, an explanatory word with academic degrees is needed

* **cmpd** --- compound nouns erroneously treated separately or as one

_Example 1:_
> **Original sentence:** Chris Patten represented British Hong Kong.
> **Machine translation:** Крис Паттен представлял британский Гонконг.
> **Edited translation:** Крис Паттен представлял Британский Гонконг.
> **Comment:** in "British Hong Kong", "British" treated as a common adjective and not a part of the named entity

_Example 2:_
> **Original sentence:** Buzz Aldrin was born in Glen Ridge New Jersey.
> **Machine translation:** Buzz Aldrin родился в Глен-Ридж-Нью-Джерси.
> **Edited translation:** Базз Олдрин родился в Глен-Ридже, Нью-Джерси.
> **Comment:** Glen Ridge New Jersey is treated as one named entity, should be as two

* **cn** --- misinterpretation of a NE as a common noun

_Example:_
> **Original sentence:** The comic character Dane Whitman Black Knight is also known as simply Dane Whitman.
> **Machine translation:** Комический персонаж Dane Whitman Black Knight также известен как просто датчанин Уитмен.
> **Edited translation:** Герой комиксов Dane Whitman Black Knight также известен как просто Дейн Уитмен.
> **Comment:** the name "Dane" translated as "a Danish"

#### 4.  **str** -  structural mistakes

* **wrdord** --- word order (usually connected with topic-comment relationships or attribution)

_Example 1:_
> **Original sentence:** Andrew the Apostle is the patron saint of Romania.
> **Machine translation:** Андрей апостол - покровитель Румынии.
> **Edited translation:** Апостол Андрей - покровитель Румынии.
> **Comment:** Andrew the Apostle ---> Apostle Andrew

_Example 2:_
> **Original sentence:** Approximately 500 employees make up the academic staff at Acharya Institute of Technology.
> **Machine translation:** Примерно 500 сотрудников составляют научные сотрудники в Ачарском технологическом институте.
> **Edited translation:** Примерно 500 сотрудников в Технологическом институте Ачарья составляют научные работники.
> **Comment:** "500 employees (in general) are the academic staff at Acharya Institute of Technology" ---> "500 employees at Acharya Institute of Technology are the academic staff"

* **del** --- deletion of the word present in the original sentence

_Example 1:_
> **Original sentence:** Bananaman the TV series was shown on the BBC.
> **Machine translation:** Бананаман телеканал был показан на BBC.
> **Edited translation:** Телесериал "Бананаман" был показан на BBC.
> **Comment:** "series" omitted

_Example 2:_
> **Original sentence:** The comic character Auron's full name is Lambien.
> **Machine translation:** Полное имя комикса Auron - Lambien.
> **Edited translation:** Полное имя героя комикса Auron - Lambien.
> **Comment:** "character" omitted

* **ins** --- erroneous insertion of an extra word, usually repetition

Insertions of qualifying words should not be marked as an error:
_Example: Pennsylvania - штат Пенсильвания (the state of Pennsylvania)_

_Example:_
> **Original sentence:** Len Wein collected an award from the Academy of Comic Book Arts.
> **Machine translation:** Лен Вейн собрал награду Академии Академии комиксов.
> **Edited translation:** Лен Уэйн получил награду Академии комиксов.
> **Comment:** Academia is repeated twice

* **cmpl** --- complex structures 
(sentences with relative, participial clauses, complex sentences;  etc. seems to be not frequent)

_Example:_
> **Original sentence:** Elliot See had a nationality that was of the United States.
> **Machine translation:** У Эллиот-Зее была национальность, принадлежавшая Соединенным Штатам.
> **Edited translation:** Эллиот Си имел гражданство Соединенных Штатов.
> **Comment:** relative clause translated as such when it should be replaced with a simpler structure

### 5. **spel** --- spelling:  wrong spelling
Wrongly transliterated named entities are tagged with "ne"
* **cap** - capitalization / no capitalization

_Example 1:_
> **Original sentence:** California have fossils of the Smilodon.
> **Machine translation:** В Калифорнии есть окаменелости "Смилодона".
> **Edited translation:** В Калифорнии есть окаменелости смилодона.
> **Comment:** smilodon is a common noun and should not be capitalized

_Example 2:_
> **Original sentence:** Marv Wolfman won the Eagle Award for comics.
> **Machine translation:** Марв Вольфман завоевал премию "орел" за комиксы.
> **Edited translation:** Марв Вольфман завоевал премию "Орел" за комиксы.
> **Comment:** the Eagle Award (премия "Орел") should be capitalized

_Example 3:_
> **Original sentence:** Elliot See died on 28 February, 1966.
> **Machine translation:** Эллиот См. Умер 28 февраля 1966 года.
> **Edited translation:** Эллиот Си умер 28 февраля 1966 года.
> **Comment:** the word "died" should not be capitalized, it is capitalized due to the wrongly inserted contraction "См." as See.

### 6. **punct** --- extraneous, wrong, omitted punctuation
usually quotation marks, commas and fulstops

_Example:_
> **Original sentence:** The comic character, Balder, was created by Jack Kirby.
> **Machine translation:** Комический персонаж, "Балдер", был создан Джеком Кирби.
> **Edited translation:** Персонаж комиксов Бальдер был создан Джеком Кёрби.
> **Comment:** commas surrounding the subject should be omitted in Russian

### 7. **rep** --- "stattering" model, repetition of a text fragment
if the number of deletions or insertions is more than 2 and a significant part of the sentence is not translated

_Example:_
> **Original sentence:** Adams County, Pennsylvania has to its west Franklin County, Pennsylvania.
> **Machine translation:** В штате Пенсильвания, штат Пенсильвания, штат Пенсильвания, штат Пенсильвания.
> **Edited translation:** На западе от округа Адамс, Пенсильвания, находится округ Франклин, Пенсильвания.
> **Comment:** repetition of "state of Pennsylvania" 3 times