# Post-editing decisions

### 1. Named entities
(from triples and other proper nouns)
All named entities should be translated according to common conventions in Russian.

#### General rules

1.1. check corresponding entry in Russian DBpedia;
1.2. if not 1.1., check translations on the Internet, choose the most common one;
1.3. in case of variation in 1.2, keep several variants;
1.4. if there is no correspondence, apply conventions for different categories;

#### Specific categories:

| Category | To do |
| ------ | ------ |
| Person's name, geographical location | always translate: search for equivalent, use rules of transliteration / transcription|
| Territorial units | use common analogies or transcriptions: *department* – департамент; *region* – регион; *avenue* – авеню; *district* – округ; *county* - графство (British), округ (American)|
| Vessels, vehicles, spacecrafts | give Russian equivalent *(e.g. Apollo-8 - "Аполлон-8")*; give translation (transcription or transliteration) or leave the English version if there is no common translation|
| Companies, institutions| translate generic name; proper name - keep English if there is no common translation; Ltd., Inc., Co. - Лтд., Инк., Ко.; acronyms - translate if common, keep English if rare. *Examples: The School of Business and Social Sciences at the Aarhus University - Школа бизнеса и социальных наук в Орхусском университете; Visvesvaraya Technical University - Технологический университет Висвесварайя*|
| Acronyms |if common - use common translation *(e.g. UN = ООН, NASA - НАСА)*; if rare - keep English acronym or use the full version *(e.g. MIT - Массачуссетский технологический институт, MIT)*|
| Work of art | usually translated, use the version in Russian if exists *(e.g. "The Good Listener" - "Особый случай")*|
| Political parties: | usually translated or transliterated *(e.g.Conservative Party - консервативная партия)*|
| University degrees, academic positions | give a common equivalent: *B.A.* - бакалавр гуманитарных наук, *B.S.* - бакалавр естественных наук; *M.S. in Engineering* -  магистр технических наук; *M.A. in Linguistics* - магистр лингвистики, магистр по специальности "Лингвистика"; *Ph.d.* - кандидат наук, but can keep Ph.D., especially for degrees received recently; Give valid explanatory Russian translation (*степень* - degree, *диплом* - diploma) for similar cases: *Уильям Андерс окончил AFIT, M.S. в 1962 году.* - *Уильям Андерс окончил AFIT с дипломом магистра в 1962 году.*|
| Events | translate only the common ones *(e.g. Olympic games - "Олимпийские игры", but keep "Burning Man", "Big 12")*, acronyms - give full version or keep the English one *(e.g. G20 - "Большая двадцатка" or G20)*|

### 2. Dates - adapt to Russian conventions:
American format YYYY-MM-DD ---> European format: DD-MM-YYYY

### 3. Auxiliary verbs and copulas: add if they are missing

### 5. Even if the sentence is grammatical, check that the lexical units correspond to each other:
*e.g. His *nationality is United States: "гражданство", but not "национальность"*

### 6. If an English sentence contains grammatical mistakes/misspellings or it is awkward, form a correct Russian sentence adhering to semantics given by RDF triples.
*Example:*
>**Original:** The comic character, Auron's full name is Lambien.
>**Machine translation:** Комический характер, полное название Auron - Lambien.
>**Edited translation:** Полное имя Аурона, героя комиксов, - Лэмбиен.

Add `comment="bad"` for the En sentence in the corpus: 
```
<lex lang="en" lid="Id3" comment="bad">The comic character, Auron's full name is Lambien.</lex>
```
## 7. If an English sentence contains individual interpretations of semantics (given by triples), keep the Russian translation as close to the initial semantics as possible. Don't tag the error.
e.g. _alter ego_ should be _alternative name_ in the translation, but we do not mark it as a translation error.
```
<lex lang="en" lid="Id2">The comic book character Blockbuster's alter ego is Roland Desmond.</lex>
<lex lang="ru" lid="Id2">Характер персонажа комиксов - альтер эго - это Роланд Десмонд.</lex>
<finallex comment="ne;voc:amb;gr:ps" lang="ru" lid="Id2">Другое имя персонажа комиксов Блокбастера - это Роланд Десмонд.</finallex>
```

## 8. Duplicate comment tags if there are two mistakes of the same kind in two different words (or named entities), give the category and subcategory if present

`comment="ne;ne;voc:amb;voc:amb"`
```
<lex lang="en" lid="Id3">The comic character, Balder's alternative name, is Balder Odinson.</lex>
<lex lang="ru" lid="Id3">Комический персонаж, альтернативное название "Балдер", - Балдер Одинсон.</lex>
<finallex comment="ne;ne;voc:amb;voc:amb" lang="ru" lid="Id3">Альтернативное имя Бальдера, героя комиксов, - Бальдер Одинсон.</finallex>
```