Postediting / Error classification bibliography

***General:***

(error classifications)

Marcello Federico, Matteo Negri, Luisa Bentivogli, Marco Turchi. Assessing the Impact of Translation Errors on Machine Translation Quality with Mixed-effects Models. Conference on Empirical Methods in Natural Language Processing. Doha, Qatar, October 2014.
https://www.matecat.com/wp-content/uploads/2014/11/1643.pdf

Valotkaite, J. (2012). Error Detection for Post-editing Rule-based Machine Translation.
https://pdfs.semanticscholar.org/b95a/1a2b62be978f65843120b16e7aeab6f00f4a.pdf?_ga=2.231572319.170644918.1555942960-348652227.1555942960

Popovic, Maja. (2018). Error Classification and Analysis for Machine Translation Quality Assessment. 10.1007/978-3-319-91241-7_7.
https://www.researchgate.net/publication/325896250_Error_Classification_and_Analysis_for_Machine_Translation_Quality_Assessment

Daems, J., Vandepitte, S., Hartsuiker, R. J., & Macken, L. (2017). Identifying the Machine Translation Error Types with the Greatest Impact on Post-editing Effort. Frontiers in psychology, 8, 1282. doi:10.3389/fpsyg.2017.01282



***Focused on English:***
Yuzhu Wang, Hai Zhao. A Light Rule-based Approach to English Subject-Verb Agreement Errors on the Third Person Singular Forms.Proceedings of the 29th Pacific Asia Conference on Language, Information and Computation: Posters. October, 2015, Shanghai, China.
https://www.aclweb.org/anthology/Y15-2040



***Focused on Russian:***
Stepanova, M. M. Analiz perevodcheskih oshibok v podgotovke prepodavatelej perevoda [Tekst] / M. M. Stepanova // Didaktika
perevoda: materialy nauchnoj konferencii / Pod red. prof. V.N. Bazyleva. – M.: Gos.IRJa im. A.S. Pushkina, 2010.

Nazarenko, O.S. Analiz oshibok studentov pri perevode pismennykh tekstov. Lingua mobilis № 1 (34), 2012
Назаренко Оксана Сергеевна Анализ ошибок студентов при переводе письменных текстов // Lingua mobilis. 2012. №1 (34). URL: https://cyberleninka.ru/article/n/analiz-oshibok-studentov-pri-perevode-pismennyh-tekstov (дата обращения: 22.04.2019).
(лингвистическая классификация)

Княжева Е. А. Переводческий анализ текста и качество перевода // Вестник ВГУ. Серия: Лингвистика и межкультурная коммуникация. 2012. №1. URL: https://cyberleninka.ru/article/n/perevodcheskiy-analiz-teksta-i-kachestvo-perevoda (дата обращения: 22.04.2019).
(общие положения, без деталей)

Болдырева Л.В. Типология переводческих ошибок англоязычной проблемной статьи на русский язык. Вестник СамГУ.2014. №5 (116)
http://vestniksamsu.ssau.ru/tgt/2014_05_124.pdf
(лингвистические и социокультурные, кратко, есть пара полезных примеров)

Комалова Л.Р. 2017.04.005-014. ОШИБКИ И НЕТОЧНОСТИ ПЕРЕВОДА. (СВОДНЫЙ РЕФЕРАТ) // Социальные и гуманитарные науки. Отечественная и зарубежная литература. Сер. 6, Языкознание: Реферативный журнал. 2017. №4. URL: https://cyberleninka.ru/article/n/2017-04-005-014-oshibki-i-netochnosti-perevoda-svodnyy-referat (дата обращения: 22.04.2019).
(много ссылок на другие подходы, обширная лингвистическая классификация)

Минченков А.Г. Этапы процесса перевода и операционные ошибки // Перевод и сопоставительная лингвистика. 2015. №11. URL: https://cyberleninka.ru/article/n/etapy-protsessa-perevoda-i-operatsionnye-oshibki (дата обращения: 22.04.2019).
(про подход к переводу и  реконцептуализацию)

Новожилова А.А. Машинные системы перевода: качество и возможности использования // Вестник ВолГУ. Серия 2: Языкознание. 2014. №3. URL: https://cyberleninka.ru/article/n/mashinnye-sistemy-perevoda-kachestvo-i-vozmozhnosti-ispolzovaniya (дата обращения: 22.04.2019).
(общие положения)

Куниловская М.А. Классификация переводческих ошибок для создания разметки в учебном параллельном корпусе Russian Learner translator Corpus // Lingua mobilis. 2013. №1 (40). URL: https://cyberleninka.ru/article/n/klassifikatsiya-perevodcheskih-oshibok-dlya-sozdaniya-razmetki-v-uchebnom-parallelnom-korpuse-russian-learner-translator-corpus (дата обращения: 22.04.2019).
(общие положения, но с фокусом на машинный перевод)
 
Карпеева О.Я. Некоторые виды грамматических преобразований при переводе с английского языка на русский // Вестник ЧГУ. 2016. №4. URL: https://cyberleninka.ru/article/n/nekotorye-vidy-grammaticheskih-preobrazovaniy-pri-perevode-s-angliyskogo-yazyka-na-russkiy (дата обращения: 22.04.2019).
(фокус на типичные преобразования)




