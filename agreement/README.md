# Inter-annotator Agreement Overview

Inter-annotator agreement was done for the examples from `part=test` in xml files.

Coders' annotations are found in the `comment` attribute. 

```xml
<lex lang="ru" lid="Id2">Абель Эрнандес сыграл за сборную Олимпийских игр в Уругвае.</lex>
<finallex comment="ne:cmpd;gr:attr" lang="ru" lid="Id2">Абель Эрнандес играл за олимпийскую сборную Уругвая.</finallex>
```

If an MT-translated sentence was correct, then `<finallex>` was left blank.

