import csv
import re
import os
from benchmark_reader_finallex_dbpedialinks import Benchmark
from sacremoses import MosesTokenizer
from sacremoses import MosesDetokenizer


def escape_spec_chars(pattern):
    # Special Regex Characters: ., +, *, ?, ^, $, (,), [, ], {,}, |
    spec_chars = ['.', '+', '*', '?', '^', '$', '(', ')', '[', ']', '{', '}']
    for char in spec_chars:
        pattern = pattern.replace(char, '\\' + char)
    return pattern


def read_rplc_dict(category, path_to_dict):
    # construct replacement dict from csv
    with open(path_to_dict + '/' + category + '_rplc_good.csv', 'r') as infile:
        reader = csv.reader(infile)
        # rplc_dict = {rows[0]: rows[1] for rows in reader}
        rplc_dict = {}
        for rows in reader:
            if rows[0] not in rplc_dict:
                rplc_dict[rows[0]] = rows[1]
            else:
                print('Shouldnt be the case. Repetitions to check', rows[0])
    return rplc_dict


def make_replacements(source, rplc_dict):
    # do replacement beginning with longer forms, track parts that were already replaced.
    target = source
    count = 0
    flags = len(source) * [False]
    for replacement_src in sorted(rplc_dict, key=len, reverse=True):  # sort by key length
        replacement_tgt = rplc_dict[replacement_src]
        # replace several occurrences one by one, and keep track of replaced parts (via flags)
        replacement_src_escaped = escape_spec_chars(replacement_src)
        regex_src = r'(^|\s)' + replacement_src_escaped + r'($|\s)'
        matches = re.finditer(regex_src, source)  # returns match objects

        for match in matches:
            index_start = match.start() + 1  # do not count possible spaces
            index_end = match.end() - 2  # do not count possible spaces

            if not flags[index_start] and not flags[index_end]:
                flags[index_start:index_end] = len(replacement_src) * [True]
                regex_tgt = r'\g<1>' + replacement_tgt + r'\g<2>'
                target = re.sub(regex_src, regex_tgt, target, count=1)
                count += 1

    return target, count


def propagate_replacements(category, global_path, out_path, path_to_dict):
    mosestok, mosesdetok = MosesTokenizer(lang='ru'), MosesDetokenizer(lang='ru')
    rplc_dict = read_rplc_dict(category, path_to_dict)
    # read all other categories
    for size in range(2, 8):
        bmk_file = 'en_ru_' + category + '_' + str(size) + '.xml'
        if os.path.isfile(global_path + bmk_file):
            b = Benchmark()
            b.fill_benchmark([(global_path, bmk_file)])
            for entry in b.entries:
                ru_lexs = [item.lex for item in entry.lexs[1::2]]  # take only Russian translations
                finallexs = entry.finallexs
                for source, finallex in zip(ru_lexs, finallexs):
                    source_tokenised = mosestok.tokenize(source, return_str=True, escape=False)
                    target, count = make_replacements(source_tokenised, rplc_dict)
                    target_detok = mosesdetok.detokenize(target.split())
                    target_detok = target_detok.replace(" \\' ", "'")
                    finallex.lex = target_detok[0].upper() + target_detok[1:]  # uppercase all the beginnings
                    if count > 0:
                        finallex.comment = ';'.join(count * ['ne'])
            b.b2xml_pretty(out_path, 'en_ru_' + category + '_' + str(size) + '.xml')
    print('Files written on disc.')


path_to_corpus = '../webnlg-ru/'
output_path = '../webnlg-ru-temp/'  # create a temporary folder to avoid writing over the existing files
rplct_dict_path = '../rule-based_pe/entities_replacements'
for category in ['University', 'Monument', 'ComicsCharacter', 'Astronaut']:
    propagate_replacements(category, path_to_corpus, output_path, rplct_dict_path)
