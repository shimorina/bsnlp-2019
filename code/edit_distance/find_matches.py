import re
from edit_distance.distance import ded


"""This code compares two strings via edit-distance and extracts modification matches. 
"""


def compare_strings(source, target):
    rs = ded(source=source.split(" "), target=target.split(" "))
    # print(rs)
    action_sequence_src = []
    src_seq_ins = []
    tgt_seq_ins = []
    for match in rs['detail']:
        # print(match)
        if not match['src_i']:
            for _ in range(len(match['tgt_i'])):
                src_seq_ins.append('INS')
                action_sequence_src.append(match['type'][0])
        if not match['tgt_i']:
            for _ in range(len(match['src_i'])):
                tgt_seq_ins.append('DEL')
        for step in match['src_i']:
            action_sequence_src.append(match['type'][0])
            src_seq_ins.append(source.split()[step])
        for step in match['tgt_i']:
            tgt_seq_ins.append(target.split()[step])
    # print(action_sequence_src)
    # print(src_seq_ins)
    # print(tgt_seq_ins)
    return action_sequence_src, src_seq_ins, tgt_seq_ins


def extract_indices(action_sequence_src):
    my_str = ''.join(action_sequence_src)
    result = len(max(re.compile(r'(n+)*').findall(my_str)))
    # print("Longest repeating sequence of none:", result)
    # print("Its index:", my_str.index(result * 'n'))
    start = my_str.index(result * 'n')
    end = start + result
    # take all subs, ins and dels on the right and on the left
    left, center, right = action_sequence_src[:start], action_sequence_src[start:end], action_sequence_src[end:]
    # print("Left", left)
    # print("Center", center)
    # print("Right", right)
    # strip off none at the very beginning and at the end
    indices_left = []
    beginning = True
    for iter, item in enumerate(left):
        if beginning:
            if item == 'n':
                continue
            else:
                indices_left.append(iter)
                beginning = False
        else:
            indices_left.append(iter)
    # print("Indices left", indices_left)
    indices_right = []
    final = True
    for iter, item in reversed(list(enumerate(right))):
        if final:
            if item == 'n':
                continue
            else:
                indices_right.append(end + iter)
                final = False
        else:
            indices_right.append(end + iter)
    indices_right = list(reversed(indices_right))
    # print("Indices right", indices_right)
    return indices_left, indices_right


def find_tokens_from_indices(indices, src_seq_ins, tgt_seq_ins, remove_insertions=True):
    # create corresponding matches from src and tgt
    src_str = []
    tgt_str = []
    for index in indices:
        src_str.append(src_seq_ins[index])
        tgt_str.append(tgt_seq_ins[index])
    if remove_insertions:
        src_str = [item for item in src_str if item != 'INS']
        tgt_str = [item for item in tgt_str if item != 'DEL']
    return src_str, tgt_str


# Some tests
# source = '" Aльба Юлия " - город в Румынии .'
# target = 'Aльба-Юлия - город в Румынии .'
# source = 'В городе Альба-Юлия есть университет под названием " 1 decembrie 1918 University " .'
# target = 'В городе Алба-Юлия есть университет под названием Университет " 1 декабря 1918 " .'
# source = 'Численность академического персонала Accademia di Archittura di Mendrisio - 100 .'
# target = 'Численность академического персонала Академии архитектуры Мендризио - 100 человек .'


def run_all(source, target):
    action_sequence_src, src_seq_ins, tgt_seq_ins = compare_strings(source, target)
    indices_left, indices_right = extract_indices(action_sequence_src)
    match_left = find_tokens_from_indices(indices_left, src_seq_ins, tgt_seq_ins)
    # print(' '.join(match_left[0]), ' '.join(match_left[1]))
    match_right = find_tokens_from_indices(indices_right, src_seq_ins, tgt_seq_ins)
    # print(' '.join(match_right[0]), ' '.join(match_right[1]))
    return match_left, match_right
