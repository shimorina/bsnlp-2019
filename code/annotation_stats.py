from agreement import read_coding
from collections import defaultdict


def annotations_stats(path, part):
    filepath1 = (path, 'en_ru_University_1.xml')
    answers_1 = read_coding(filepath1, part)[1]

    filepath3 = (path, 'en_ru_ComicsCharacter_1.xml')
    answers_3 = read_coding(filepath3, part)[1]

    filepath2 = (path, 'en_ru_Monument_1.xml')
    answers_2 = read_coding(filepath2, part)[1]

    filepath4 = (path, 'en_ru_Astronaut_1.xml')
    answers_4 = read_coding(filepath4, part)[1]

    errors = answers_1 + answers_2 + answers_3 + answers_4

    print('Comment count', len(errors))
    freq_dict = defaultdict(int)
    for item in errors:
        # without subcategories
        try:
            item = item.split(':')[0]
        except:
            pass
        freq_dict[item] += 1
    print(freq_dict)


path_to_corpus = '../webnlg-ru/'
part = 'all'  # can be 'test', 'dev', 'train', or 'all'
annotations_stats(path_to_corpus, 'all')

# Output:
# all 4 categories (University, Monument, ComicsCharacter, Astronaut)
# 1722 comments
# {'ne': 917 (53%), 'gr': 295 (17%), 'voc': 238 (14%), 'str': 193 (11%), 'punct': 63 (4%), 'spel': 4, 'ntr': 12})
