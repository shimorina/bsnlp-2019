import csv
from benchmark_reader_finallex_dbpedialinks import Benchmark
from sacremoses import MosesTokenizer
from edit_distance.find_matches import run_all


def create_rplcmnt_file(category, path_to_file, out_path):
    """
    Compare MT and PE, extract matches, write them to csv.
    :param category: DBpedia category
    :return:
    """
    b = Benchmark()
    b.fill_benchmark([(path_to_file, 'en_ru_' + category + '_1.xml')])
    comparisons = []
    replacements = set()
    mosestok = MosesTokenizer(lang='ru')
    count_lexs = 0
    count_repls = 0
    for entry in b.entries:
        lexs_ru_mt = entry.lexs[1::2]  # take even elements
        lexs_ru_pe = entry.finallexs
        assert len(lexs_ru_mt) == len(lexs_ru_pe), print(entry.id)
        for lex_mt, lex_pe in zip(lexs_ru_mt, lexs_ru_pe):
            count_lexs += 1
            if lex_pe.lex:  # empty pe field means mt translation was correct
                count_repls += 1
                identifier = entry.id + '-' + lex_mt.id
                mt = mosestok.tokenize(lex_mt.lex, return_str=True, escape=False)
                pe = mosestok.tokenize(lex_pe.lex, return_str=True, escape=False)
                match_left, match_right = run_all(mt, pe)
                if match_left[0]:
                    comparisons.append([identifier, mt, pe, ' '.join(match_left[0]), ' '.join(match_left[1])])
                    replacements.add((' '.join(match_left[0]), ' '.join(match_left[1])))
                if match_right[0]:
                    comparisons.append([identifier, mt, pe, ' '.join(match_right[0]), ' '.join(match_right[1])])
                    replacements.add((' '.join(match_right[0]), ' '.join(match_right[1])))
    with open(out_path + '/' + category + '.csv', 'w+') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(comparisons)
    with open(out_path + '/' + category + '_rplc.csv', 'w+') as csvfile:
        csvwriter = csv.writer(csvfile)
        csv_rplc = [list(item) for item in sorted(list(replacements), key=lambda tup: tup[0])]
        csvwriter.writerows(csv_rplc)
    print('Texts', count_lexs)
    print('Replacements', count_repls)


path_to_file = '../webnlg-ru/'
output_path='../rule-based_pe/entities_replacements/'

create_rplcmnt_file('University', path_to_file, output_path)   # lexs: 220, repls: 163
create_rplcmnt_file('Monument', path_to_file, output_path)   # lexs: 201, repls: 153
create_rplcmnt_file('ComicsCharacter', path_to_file, output_path)  # lexs: 302, repls: 264
create_rplcmnt_file('Astronaut', path_to_file, output_path)    # lexs: 353, repls: 276

# total: 1076, 856
