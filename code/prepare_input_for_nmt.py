import os
import random
from sacremoses import MosesTokenizer
from benchmark_reader_finallex_dbpedialinks import Benchmark


"""
This code does the following:
# read files from webnlg-ru/
# create files: tokenised and not tokenised for .mt .pr .en
# calculate how many instances are in dev, train, test
# calculate the proportion of post-edits
"""


def collect_triplets(entry):
    global count
    triplets = []
    lexs_en = [item.lex for item in entry.lexs[0::2]]
    lexs_ru_mt = [item.lex for item in entry.lexs[1::2]]
    lexs_ru_pe = [item.lex for item in entry.finallexs]
    for lex_en, lex_mt, lex_pe in zip(lexs_en, lexs_ru_mt, lexs_ru_pe):
        if not lex_pe:
            lex_pe = lex_mt
        triplets.append((lex_en, lex_mt, lex_pe))
        if lex_mt != lex_pe:
            count += 1
        if '\n' in lex_pe:
            print('To fix', lex_pe)
    return triplets


def write_to_file(corpus, part, path_out):
    mosestok_ru, mosestok_en = MosesTokenizer(lang='ru'), MosesTokenizer(lang='en')
    en = [item[0] for item in corpus]
    mt = [item[1] for item in corpus]
    pe = [item[2] for item in corpus]
    assert len(en) == len(mt) == len(pe)
    # shuffle three lists in the same way
    random.seed(10)
    c = list(zip(en, mt, pe))
    random.shuffle(c)
    en, mt, pe = zip(*c)
    # create tokenised data
    en_tokenised = [mosestok_en.tokenize(item, return_str=True, escape=False) for item in en]
    mt_tokenised = [mosestok_ru.tokenize(item, return_str=True, escape=False) for item in mt]
    pe_tokenised = [mosestok_ru.tokenize(item, return_str=True, escape=False) for item in pe]
    with open(path_out + part + '.en', 'w+') as f:
        f.write('\n'.join(en))
    with open(path_out + part + '.tok.en', 'w+') as f:
        f.write('\n'.join(en_tokenised))
    with open(path_out + part + '.mt', 'w+') as f:
        f.write('\n'.join(mt))
    with open(path_out + part + '.tok.mt', 'w+') as f:
        f.write('\n'.join(mt_tokenised))
    with open(path_out + part + '.pe', 'w+') as f:
        f.write('\n'.join(pe))
    with open(path_out + part + '.tok.pe', 'w+') as f:
        f.write('\n'.join(pe_tokenised))


def prepare_files(global_path, path_out):
    train = []
    dev = []
    test = []
    for category in categories:
        for size in range(1, 8):
            bmk_file = 'en_ru_' + category + '_' + str(size) + '.xml'
            # print(bmk_file)
            if os.path.isfile(global_path + bmk_file):
                b = Benchmark()
                b.fill_benchmark([(global_path, bmk_file)])
                for entry in b.entries:
                    if entry.part == 'train':
                        train += collect_triplets(entry)
                    elif entry.part == 'dev':
                        dev += collect_triplets(entry)
                    elif entry.part == 'test':
                        test += collect_triplets(entry)
    write_to_file(train, 'train', path_out)
    write_to_file(dev, 'dev', path_out)
    write_to_file(test, 'test', path_out)
    print('Train', len(train))
    print('Dev', len(dev))
    print('Test', len(test))
    print('Post-edited translations', count)


path = '../webnlg-ru/'
categories = ['Astronaut', 'ComicsCharacter', 'Monument', 'University']
out_path = '../ape/opennmt_inputs/'

count = 0  # how many translations were post-edited
prepare_files(path, out_path)


# Train 4174
# Dev 528
# Test 483
# Post-edited translations 4188 (now 4200)

# if you rerun this program on the current data,
# then there'll be some changes in produced files comparing to those available in this repo.
# That happens because corpus files were slightly modified afterwards.
