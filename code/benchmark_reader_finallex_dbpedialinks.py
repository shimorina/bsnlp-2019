import xml.etree.ElementTree as Et
from collections import defaultdict
from os import listdir
import json
from lxml import etree


class Triple:

    def __init__(self, s, p, o):
        self.s = s
        self.o = o
        self.p = p


class Tripleset:

    def __init__(self):
        self.triples = []
        self.clusterid = 0

    def fill_tripleset(self, t):
        for xml_triple in t:
            s, p, o = xml_triple.text.split('|')
            triple = Triple(s, p, o)
            self.triples.append(triple)


class Lexicalisation:

    def __init__(self, lex, lang, lid, comment='', parse=[]):
        self.lex = lex
        self.lang = lang
        self.id = lid
        self.comment = comment
        self.parse = parse


class Entry:

    def __init__(self, category, size, eid, shape, shape_type, part):
        self.originaltripleset = []
        self.modifiedtripleset = Tripleset()
        self.lexs = []
        self.finallexs = []
        self.dbpedialinks = []
        self.category = category
        self.size = size
        self.id = eid
        self.shape = shape
        self.shape_type = shape_type
        self.part = part

    def fill_originaltriple(self, xml_t):
        otripleset = Tripleset()
        self.originaltripleset.append(otripleset)   # multiple originaltriplesets for one entry
        otripleset.fill_tripleset(xml_t)

    def fill_modifiedtriple(self, xml_t):
        self.modifiedtripleset.fill_tripleset(xml_t)

    def create_lex(self, xml_lex):
        lang = xml_lex.attrib['lang']
        lid = xml_lex.attrib['lid']
        lex = Lexicalisation(xml_lex.text, lang, lid)
        self.lexs.append(lex)

    def create_finallex(self, xml_finallex):
        lang = xml_finallex.attrib['lang']
        lid = xml_finallex.attrib['lid']
        comment = xml_finallex.attrib['comment']
        lex = Lexicalisation(xml_finallex.text, lang, lid, comment)
        self.finallexs.append(lex)

    def create_dbpedialinks(self, xml_dbpedialinks):
        for xml_dblink in xml_dbpedialinks:
            s, p, o = xml_dblink.text.split('|')
            dbp_link = Triple(s, p, o)
            self.dbpedialinks.append(dbp_link)

    def count_lexs(self):
        return len(self.lexs)

    def flat_tripleset(self):
        """
        Render modified triples to the flat representation with <br>.
        :return: flat representation
        """
        flat_mr = []
        for triple in self.modifiedtripleset.triples:
            flat_triple = triple.s + '|' + triple.p + '|' + triple.o
            flat_mr.append(flat_triple)
        if self.size == '1':
            return flat_mr[0]
        else:
            return '<br>'.join(flat_mr)

    def relations(self):
        """
        Give a set of properties found in tripleset
        :return: set of properties
        """
        # rel_set = tuple()
        rel_set= set()
        for triple in self.modifiedtripleset.triples:
            # rel_set += triple.p,
            rel_set.add(triple.p)
        return rel_set

    def list_triples(self):
        """
        Return a list of triples for an entry.
        :return: list of triples
        """
        triples = []
        for triple in self.modifiedtripleset.triples:
            flat_triple = triple.s + '|' + triple.p + '|' + triple.o
            triples.append(flat_triple)
        return triples


class Benchmark:

    def __init__(self):
        self.entries = []

    def fill_benchmark(self, fileslist):
        """

        :param fileslist: [(path_to_file, filename.xml), (), ... ()]
        :return:
        """
        for file in fileslist:
            # print(file[1])
            myfile = file[0] + '/' + file[1]
            tree = Et.parse(myfile)
            root = tree.getroot()
            for xml_entry in root.iter('entry'):
                # ignore triples with no lexicalisations
                lexfound = False
                for child in xml_entry:
                    if child.tag == "lex":
                        lexfound = True
                        break
                if lexfound is False:
                    continue

                entry_id = xml_entry.attrib['eid']
                category = xml_entry.attrib['category']
                size = xml_entry.attrib['size']
                part = xml_entry.attrib['part']
                try:
                    shape = xml_entry.attrib['shape']
                    shape_type = xml_entry.attrib['shape_type']
                except KeyError:
                    shape = ''
                    shape_type = ''
                entry = Entry(category, size, entry_id, shape, shape_type, part)
                for child in xml_entry:
                    if child.tag == 'originaltripleset':
                        entry.fill_originaltriple(child)
                    elif child.tag == 'modifiedtripleset':
                        entry.fill_modifiedtriple(child)
                    elif child.tag == 'lex':
                        entry.create_lex(child)
                    elif child.tag == 'finallex':
                        entry.create_finallex(child)
                    elif child.tag == 'dbpedialinks':
                        entry.create_dbpedialinks(child)
                self.entries.append(entry)

    def add_parses(self, fileslist):
        for f in fileslist:
            tree = Et.parse(f)
            root = tree.getroot()
            filename = root.attrib['bmfile']
            sizetriples, category, *other = filename.split('_')  # 2triples_Artist_train_release.xml
            size = sizetriples[0]
            if size == '1':
                category = filename.split('_')[2]  # 1triple_allSolutions_Airport_test_release_parsed.xml
            for parse in root.iter('parse'):
                entry_id = parse.attrib['eid']
                lex_id = int(parse.attrib['lid'][2:])
                dep_sentences = []
                for sentence in parse:
                    depset = [dep.text for dep in sentence]
                    # there are empty sentences: <depset sid="0"/>
                    if depset:
                        dep_sentences.append(depset)
                # add dependencies to a lexicalisation of an entry in the benchmark
                for entry in self.entries:
                    if entry.category == category and entry.size == size:
                        if entry.id == entry_id:
                            # print(entry_id)
                            entry.lexs[lex_id - 1].parse = dep_sentences
                            break

    def total_lexcount(self):
        count = [entry.count_lexs() for entry in self.entries]
        return sum(count)

    def unique_p(self):
        properties = [triple.p for entry in self.entries for triple in entry.modifiedtripleset.triples]
        return len(set(properties))

    def entry_count(self, size=None, cat=None):
        """
        calculate the number of entries in benchmark
        :param size: size (should be string)
        :param cat: category
        :return: entry count
        """
        if not size and cat:
            entries = [entry for entry in self.entries if entry.category == cat]
        elif not cat and size:
            entries = [entry for entry in self.entries if entry.size == size]
        elif not size and not cat:
            return len(self.entries)
        else:
            entries = [entry for entry in self.entries if entry.category == cat and entry.size == size]
        return len(entries)

    def lexcount_size_category(self, size='', cat=''):
        count = [entry.count_lexs() for entry in self.entries if entry.category == cat and entry.size == size]
        return len(count)

    def property_map(self):
        mprop_oprop = defaultdict(set)
        for entry in self.entries:
            for tripleset in entry.originaltripleset:
                for i, triple in enumerate(tripleset.triples):
                    mprop_oprop[entry.modifiedtripleset.triples[i].p].add(triple.p)
        return mprop_oprop

    def filter(self, size=[], cat=[]):
        """
        Filter set of entries in the benchmark wrt size and category.
        :param size: list of triple sizes to extract; default empty -- all sizes
        :param cat: list of categories to extract; default empty -- all categories
        :return: copied benchmark object with filtered size and categories;
                if no entry is left, return None
        """
        bench_filtered = self.copy()
        for entry in self.entries:
            deleted = False
            if cat:
                if entry.category not in cat:
                    bench_filtered.del_entry(entry)
                    deleted = True
            if size and not deleted:
                if entry.size not in size:
                    bench_filtered.del_entry(entry)
        if bench_filtered.entries:
            return bench_filtered
        else:
            return None

    def copy(self):
        """
        Copy benchmark.
        :return: a copy list
        """
        b_copy = Benchmark()
        b_copy.entries = list(self.entries)
        return b_copy

    def triplesets(self):
        """
        List of all triplesets.
        :return: a list of objects Tripleset
        """
        all_triplesets = [entry.modifiedtripleset for entry in self.entries]
        return all_triplesets

    def del_entry(self, entry):
        self.entries.remove(entry)

    def get_lex_by_id(self, entry_category, entry_size, entry_id, lex_id):
        """Get lexicalisation by supplying entry and lex ids."""
        for entry in self.entries:
            if entry.id == entry_id and entry.size == entry_size and entry.category == entry_category:
                for lex in entry.lexs:
                    if lex.id == lex_id:
                        return lex.lex

    def subjects_objects(self):
        subjects = set()
        objects = set()
        for entry in self.entries:
            for triple in entry.modifiedtripleset.triples:
                subjects.add(triple.s)
                objects.add(triple.o)
        return subjects, objects

    def b2json(self, path, filename):
        """Convert benchmark to json."""
        data = {}
        data['entries'] = []
        entry_id = 0  # new entry ids
        for entry in self.entries:
            entry_id += 1
            orig_triplesets = {}
            orig_triplesets['originaltripleset'] = []
            modif_tripleset = []
            lexs = []
            for otripleset in entry.originaltripleset:
                orig_tripleset = []
                for triple in otripleset.triples:
                    orig_tripleset.append({'subject': triple.s, 'property': triple.p, 'object': triple.o})
                orig_triplesets['originaltripleset'].append(orig_tripleset)

            for triple in entry.modifiedtripleset.triples:
                modif_tripleset.append({'subject': triple.s, 'property': triple.p, 'object': triple.o})

            for lex in entry.lexs:
                lexs.append({'comment': lex.comment, 'xml_id': lex.id, 'lex': lex.lex})

            data['entries'].append({entry_id : {'category': entry.category, 'size': entry.size, 'xml_id': entry.id,
                                                'shape': entry.shape, 'shape_type': entry.shape_type,
                                                'originaltriplesets': orig_triplesets,
                                                'modifiedtripleset': modif_tripleset,
                                                'lexicalisations': lexs}
                                    })

        with open(path + '/' + filename, 'w+', encoding='utf8') as outfile:
            json.dump(data, outfile, ensure_ascii=False, indent=4, sort_keys=True)

    def lexicalisations(self):
        """

        :return: all objects Lexicalisation
        """
        all_lexs = []
        for entry in self.entries:
            all_lexs.extend(entry.lexs)  # append all the items from the iterable
        all_lexs = [item for item in all_lexs]
        return all_lexs

    def b2xml(self, path, filename):
        """Convert benchmark to xml."""
        root = Et.Element('benchmark')
        entries_xml = Et.SubElement(root, 'entries')
        for entry in self.entries:
            entry_xml = Et.SubElement(entries_xml, 'entry',
                                      attrib={'category': entry.category, 'eid': entry.id, 'part': entry.part,
                                              'size': entry.size})
            mtripleset_xml = Et.SubElement(entry_xml, 'modifiedtripleset')
            for mtriple in entry.modifiedtripleset.triples:
                mtriple_xml = Et.SubElement(mtripleset_xml, 'mtriple')
                mtriple_xml.text = mtriple.s + '|' + mtriple.p + '|' + mtriple.o
            for lex in entry.lexs:
                lex_xml = Et.SubElement(entry_xml, 'lex', attrib={'lang': lex.lang, 'lid': lex.id})
                lex_xml.text = lex.lex
            for lex in entry.finallexs:
                lex_xml = Et.SubElement(entry_xml, 'finallex', attrib={'comment': '', 'lang': lex.lang, 'lid': lex.id})
                lex_xml.text = lex.lex
            if entry.dbpedialinks:  # if they exist
                dbpedialinks_xml = Et.SubElement(entry_xml, 'dbpedialinks')
                for link in entry.dbpedialinks:
                    dbpedialink_xml = Et.SubElement(dbpedialinks_xml, 'dbpedialink')
                    dbpedialink_xml.text = link.s + '|' + link.p + '|' + link.o

        tree = Et.ElementTree(root)
        tree.write(path + '/' + filename, encoding='utf-8', xml_declaration=True)

    def b2xml_pretty(self, path, filename):
        """
        Convert benchmark to pretty xml.
        Need to use the lxml library.
        """
        root = etree.Element('benchmark')
        entries_xml = etree.SubElement(root, 'entries')
        for entry in self.entries:
            entry_xml = etree.SubElement(entries_xml, 'entry',
                                         attrib={'category': entry.category, 'eid': entry.id, 'part': entry.part,
                                                 'size': entry.size})
            mtripleset_xml = etree.SubElement(entry_xml, 'modifiedtripleset')
            for mtriple in entry.modifiedtripleset.triples:
                mtriple_xml = etree.SubElement(mtripleset_xml, 'mtriple')
                mtriple_xml.text = mtriple.s + '|' + mtriple.p + '|' + mtriple.o
            for count, lex in enumerate(entry.lexs):
                lex_xml = etree.SubElement(entry_xml, 'lex', attrib={'lang': lex.lang, 'lid': lex.id})
                lex_xml.text = lex.lex
                if count%2 != 0:
                    finallex = entry.finallexs[int((count + 1) / 2) - 1]
                    finallex_xml = etree.SubElement(entry_xml, 'finallex', attrib={'comment': finallex.comment, 'lang': finallex.lang, 'lid': finallex.id})
                    if finallex.lex:
                        finallex_xml.text = finallex.lex
                    else:
                        finallex_xml.text = ''
            if entry.dbpedialinks:  # if they exist
                dbpedialinks_xml = etree.SubElement(entry_xml, 'dbpedialinks')
                for link in entry.dbpedialinks:
                    dbpedialink_xml = etree.SubElement(dbpedialinks_xml, 'dbpedialink', attrib={'direction': 'en2ru'})
                    dbpedialink_xml.text = link.s + '|' + link.p + '|' + link.o

        # write xml file
        with open(path + '/' + filename, 'wb') as f:
            content = etree.tostring(root, method='xml', encoding='utf-8', xml_declaration=True,
                                     pretty_print=True)
            f.write(content)

    @staticmethod
    def categories():
        return ['Airport', 'Artist', 'Astronaut', 'Athlete', 'Building', 'CelestialBody', 'City',
                'ComicsCharacter', 'Food', 'MeanOfTransportation', 'Monument',
                'Politician', 'SportsTeam', 'University', 'WrittenWork']


def select_files(topdir, category='', size=(1, 8)):
    finaldirs = [topdir+'/'+str(item)+'triples' for item in range(size[0], size[1])]

    finalfiles = []
    for item in finaldirs:
        finalfiles += [(item, filename) for filename in sorted(listdir(item)) if category in filename and '.xml' in filename]
    return finalfiles



