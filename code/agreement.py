from sklearn.metrics import cohen_kappa_score
from benchmark_reader_finallex_dbpedialinks import Benchmark
from natsort import natsorted
import csv


"""
This code does the following:
- read coders' comments
- validate them
- if double ne, count them twice
- calculate iaa (Cohen's kappa)
- create .csv for each error type (lex, gr, ne)
"""

error_types = {'gr', 'voc', 'ne', 'str', 'spel', 'norm', 'punct', 'stl', 'rep', 'ntr'}


class AgreementUnit:

    def __init__(self, comment, text_ru_orig, text_ru_pe, text_en, text_id):
        self.comment = comment
        self.text_ru_orig = text_ru_orig
        self.text_ru_pe = text_ru_pe
        self.text_en = text_en
        self.text_id = text_id


def validate(errors):
    """
    To prevent typos in annotation, check if error types exist, check if error subtypes exist.
    :param errors: list of errors, e.g. [ne, gr:ga, ne]
    :return:
    """
    error_subtypes = {'gr:cm', 'gr:cop', 'gr:have', 'gr:asp', 'gr:pr', 'gr:attr', 'gr:ps', 'gr:pos',
                      'gr:ga', 'gr:v', 'gr:self',
                      'voc:amb', 'voc:colloc', 'voc:scope', 'voc:cont', 'voc:term', 'voc:var', 'voc:wr',
                      'ne:wt', 'ne:nt', 'ne:abr', 'ne:exp', 'ne:cmpd', 'ne:cn',
                      'str:wrdord', 'str:del', 'str:ins', 'str:caus', 'str:cmpl', 'spel:cap'}
    for error in errors:
        if ':' not in error:
            if error not in error_types:
               print('Annotation problem', error)
        else:
            if error not in error_subtypes:
                print('Annotation problem', error)


def split_comment(comment):
    # ne;gr:ga;ne
    # print(comment)
    comment = comment.replace(' ', '')  # just in case
    if ';' not in comment:
        validate([comment])
        return [comment]
    else:
        errs = comment.split(';')
        validate(errs)
        return sorted(errs)


def read_coding(file_coder, part='test'):
    """
    Read coder file.
    :param file_coder: a tuple (path, filename)
    :param part: dataset part (train, dev, test). Pass 'all' to have all the corpus.
    :return: a list of agreement units; a list of all errors encountered
    """
    b_coder = Benchmark()
    b_coder.fill_benchmark([file_coder])
    answers = []
    all_errors = []
    for entry in b_coder.entries:
        lexs = entry.lexs
        finallexs = entry.finallexs
        if entry.part == part or part == 'all':  # coding was done only on dev / test
        # iterating over every two elements
            for en_lex, ru_lex, ru_lex_pe in zip(lexs[0::2], lexs[1::2], finallexs):
                comment = ru_lex_pe.comment
                pe_id = entry.id + '-' + en_lex.id
                # print(ru_lex_pe.lex)
                if comment:
                    errors = split_comment(comment)
                    all_errors += errors
                    # propagate comments
                    for error in errors:
                        # comment, ru, ru_pe, en, id
                        ag_unit = AgreementUnit(error, ru_lex.lex, ru_lex_pe.lex, en_lex.lex, pe_id)
                        answers.append(ag_unit)
                else:
                    ag_unit = AgreementUnit('0', ru_lex.lex, ru_lex_pe.lex, en_lex.lex, pe_id)
                    answers.append(ag_unit)
    return answers, all_errors


def compare_coders(part, path, coder1_file, coder2_file, out_path):
    """
    Calculate IIA, write answers to files for each category.
    :param part: attribute in xml files (test, dev, train). Annotations were done on test.
    :param path: path to coders' files
    :param coder1_file: filename of coder1
    :param coder2_file: filename of coder2
    :param out_path: output path
    :return:
    """
    filepath1 = (path, coder1_file)
    answers_1, _ = read_coding(filepath1)
    filepath2 = (path, coder2_file)
    answers_2, _ = read_coding(filepath2)
    # collect all ids
    ids = set([item.text_id for item in answers_1])  # have all ids
    print('total units initial', len(ids))
    for err_type in sorted(error_types):
        # filter all answers that has ne or 0
        answers_1_filtered = [unit for unit in answers_1 if err_type in unit.comment or '0' in unit.comment]
        answers_1_filtered_ids = [unit.text_id for unit in answers_1_filtered]
        answers_2_filtered = [unit for unit in answers_2 if err_type in unit.comment or '0' in unit.comment]
        answers_2_filtered_ids = [unit.text_id for unit in answers_2_filtered]
        # print(len(answers_1_filtered))
        # print(len(answers_2_filtered))
        for text_id in ids:
            # loop through all ids, if not present, add an empty answer
            if text_id not in answers_1_filtered_ids:
                answers_1_filtered.append(AgreementUnit('0', '', '', '', text_id))
            if text_id not in answers_2_filtered_ids:
                answers_2_filtered.append(AgreementUnit('0', '', '', '', text_id))
        # print(len(answers_1_filtered))
        # print(len(answers_2_filtered))
        # if coder1 has more units than the other, add 0;
        for text_id in ids:
            text_id_count_coder1 = [unit.text_id for unit in answers_1_filtered if unit.text_id == text_id]
            text_id_count_coder2 = [unit.text_id for unit in answers_2_filtered if unit.text_id == text_id]
            difference = len(text_id_count_coder1) - len(text_id_count_coder2)
            if difference < 0:
                # coder 2 has more units
                for _ in range(abs(difference)):
                    answers_1_filtered.append(AgreementUnit('0', '', '', '', text_id))
            elif difference > 0:
                # coder 1 has more units
                for _ in range(difference):
                    answers_2_filtered.append(AgreementUnit('0', '', '', '', text_id))
        # sort by id
        answers_1_filtered = natsorted(answers_1_filtered, key=lambda x: x.text_id)
        answers_2_filtered = natsorted(answers_2_filtered, key=lambda x: x.text_id)

        # assert the same number of units
        assert len(answers_1_filtered) == len(answers_2_filtered), 'Coders must have the same number of units'
        print('total units category', err_type, len(answers_2_filtered))

        # inter-annotator agreement
        answers_1_only = [unit.comment for unit in answers_1_filtered]
        answers_2_only = [unit.comment for unit in answers_2_filtered]
        kappa = round(cohen_kappa_score(answers_1_only, answers_2_only), 3)
        print('Kappa (with subcats):', kappa)
        # remove subcategories
        for iterat, unit in enumerate(answers_1_only):
            if ':' in unit:
                answers_1_only[iterat] = unit.split(':')[0]
        for iterat, unit in enumerate(answers_2_only):
            if ':' in unit:
                answers_2_only[iterat] = unit.split(':')[0]
        kappa = round(cohen_kappa_score(answers_1_only, answers_2_only), 3)
        print('Kappa (without subcats):', kappa)

        # save to csv
        with open(out_path + part + '/' + err_type + '.csv', 'w+') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['id', 'coder1', 'coder2', 'ru_pe_coder1', 'ru_pe_coder2', 'ru_orig', 'en_orig'])
            for unit_1, unit_2 in zip(answers_1_filtered, answers_2_filtered):
                # find not empty texts
                if unit_2.text_ru_orig:
                    csvwriter.writerow([unit_1.text_id, unit_1.comment, unit_2.comment,
                                        unit_1.text_ru_pe, unit_2.text_ru_pe, unit_2.text_ru_orig, unit_2.text_en])
                else:
                    csvwriter.writerow([unit_1.text_id, unit_1.comment, unit_2.comment,
                                        unit_1.text_ru_pe, unit_2.text_ru_pe, unit_1.text_ru_orig, unit_1.text_en])


agreement_data = '../agreement'
coder1_file = 'nematus_Athlete_1_coder1.xml'
coder2_file = 'nematus_Athlete_1_coder2.xml'
output_folder = '../agreement/Athlete_'  # 'test' will be appended

compare_coders('test', '../agreement', coder1_file, coder2_file, output_folder)
